<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - FAQs</title>
</head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="yellow-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="transparent-box">
                    <h1>Frequently Asked Questions</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            Frequently Asked Questions<br>
            <i>General</i>
        </div>
        <div class="paragraph">
            <div class="heading">
                Where are your servers located?
            </div>
            <div class="text">
                All the services listed on this site are located in Australia. Gaming servers, voice and dedicated
                services are located in Melbourne (NEXTDC M1), whilst website hositng is located in
                Sydney (Equinix SY3).
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                What are the specs of your servers?
            </div>
            <div class="text">
                We use a wide range of servers, which vary in their specifications. However, all our servers run either
                a E5 or E7 intel chipset at a minimum clock-speed of 2.4GHz, with a minimum of 32GB of DDR3 RAM.
                Moreover, all our servers hold their storage in a RAID-10 array.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                How long does it take to setup?
            </div>
            <div class="text">
                Setup is an instant process, once payment has been received and finalised.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Do your servers have DDOS Protection?
            </div>
            <div class="text">
                The term DDOS stands for Distributed Denial of Service Attack. DDOS attacks is an act of making a
                service unavailable for a period of time. All our servers are equipped with filtering devices.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Do your servers have DDOS Protection?
            </div>
            <div class="text">
                The term DDOS stands for Distributed Denial of Service Attack. DDOS attacks is an act of making a
                service unavailable for a period of time. All our servers are equipped with filtering devices.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Is all server data backed-up?
            </div>
            <div class="text">
                All our server run in a RAID-10 Array, which ensures redundancy and speed is upheld.
                Thus if a drive were to fail, minimising downtime and ensuring that no data has been lost. An external
                backup is provided on some services. Please contact support to enquire whether it is included in your
                service.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Am I able to cancel my service?
            </div>
            <div class="text">
                We don’t have any type of contracts, pay monthly, quarterly, semiannually or annually and we’ll
                always give you the option to cancel. Also, did you know that we have a 48 hour money back guarantee on
                all our services? No matter the reason, open a support ticket and we'll refund the amount paid*.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                What payment methods can I use?
            </div>
            <div class="text">
                We accept PayPal, all major credit/debit cards (Mastercard, Visa and American Express) via Stripe and
                G2APay.
            </div>
        </div>
        <hr>
        <div class="content-title">
            <i>Minecraft / Gaming Services</i>
        </div>
        <div class="paragraph">
            <div class="heading">
                Do you allow the use of any mod packs and custom jars?
            </div>
            <div class="text">
                Yes, we allow you to use any jar that you wish to use. This is due to us providing full FTP file
                access to your server. Did you know that we also provide an instant mod-pack installer? Select from
                hundreds of mods/server-types available, and it'll install within seconds.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                What Java version do you use?
            </div>
            <div class="text">
                All our servers use Java 8. This version is backwards-compatible with Java 7.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Do you provide MySQL database access?
            </div>
            <div class="text">
                We absolutely do! We provide MYSQL databases free of charge.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Can I use bungeecord?
            </div>
            <div class="text">
                Yes, you are able to install bungeecord onto any of our servers. Did you know that we provide server
                management at a small cost? We'll complete any task that you set us related to your server.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Do I get any freebies with my game server?
            </div>
            <div class="text">
                Yes! You receive a Buycraft 30 Day premium mebership, Enjin 45 Day premium membership, free website
                hosting. Servers greater than 4GB of RAM/equivalent, receive a free dedicated IP address.
            </div>
        </div>
        <hr>
        <div class="content-title">
            <i>Dedicated Servers</i>
        </div>
        <div class="paragraph">
            <div class="heading">
                How long does it take to provision?
            </div>
            <div class="text">
                Provisioning the dedicated server takes around 12 - 48 hours. Don't worry, your next invoice date
                will be adjusted accordingly to when the server was provisioned.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Can I get a location other than Melbourne?
            </div>
            <div class="text">
                Yes, we can provision a server in any other data centre located in Australia. However, the cost may be
                subject to change as a result of server transport costs, colocation fees and administration fees.
            </div>
        </div>
    </div>
</section>


<?php include("assets/footer.inc"); ?>
