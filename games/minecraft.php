<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - Minecraft</title>
</head>

<?php include("../assets/header.inc"); ?>
<?php include("../assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="minecraft">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="lighter-transparent-box">
                    <h1>Minecraft</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="plan-list">
            <div class="row">

                <?php
                $i = 0;
                $plans = array(
                    array("512mb","$5.00","10 Slots","standard-plan-1","#"),
                    array("1gb","$10.00","20 Slots","standard-plan-2","#"),
                    array("2gb","$20.00","30 Slots","standard-plan-3","#"),
                    array("3gb","$30.00","40 Slots","standard-plan-4","#"),
                    array("4gb","$40.00","50 Slots","standard-plan-5","#"),
                    array("5gb","$50.00","60 Slots","standard-plan-6","#"),
                    array("6gb","$60.00","70 Slots","standard-plan-7","#"),
                    array("8gb","$80.00","90 Slots","standard-plan-8","#"),
                    array("10gb","$100.00","110 Slots","standard-plan-9","#"),
                );

                while ($i < count($plans)) {
                    echo '
                <div class="col-xs-10 col-xs-offset-1 col-lg-4 col-md-6">
                    <div class="plan">
                        <div class="plan-name" id="'.$plans[$i][3].'">
                            '.$plans[$i][0].'
                        </div>
                        <div class="plan-details">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-4 col-6">
                                    <h3>Price</h3>
                                    <p>'.$plans[$i][1].'</p>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-4 col-6">
                                    <h3>Slots</h3>
                                    <p>'.$plans[$i][2].'</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4" style="padding: 0;">
                                    <button class="button slim-button">Buy Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                ';
                    $i++;
                };
                ?>
            </div>
        </div>
        <!--
        TODO - Fix Outline-List-Box
        <div class="outline-list-box">
            <div class="container">
                <div class="outline-list">
                    <ul>
                        <li>10 MySQL Databases</li>
                        <li>Unlimited Storage</li>
                        <li>24/7 Minecraft Support</li>
                        <li>One Click Mod Install</li>
                        <li>Buycraft Ultimate Trial</li>
                        <li>Enjin Advanced Trial</li>
                        <li>Dedicated IP Address*</li>
                        <li>Remote Backups</li>
                    </ul>
                </div>
            </div>
        </div>-->
        <hr>
        <div class="row">

            <?php
            $i = 0;
            $features = array(
                array("Instant Setup", "Your game server will be setup straight after payment. Don't wait for 
                    bureaucracy, play instantly.", "bt-bolt"),
                array("DDOS Protection", "Full DDOS protection and mitigation is included on all games to minimise any 
                    downtime and maximise game time.", "bt-shield"),
                array("1-Click Mod Installer", "Enjoy the ease of installing hundreds of game modifications 
                    instantaneously at a single click.", "bt-download"),
                array("Full Control", "We give you full control, customise every aspect from the gamemode it runs to 
                    the IP you connect to.", "bt-crown"),
                array("Awesome Support", "We're available to 24/7 to answer any questions or problems that you may have. 
                    We've got your back!", "bt-wrench"),
                array("Money-back Guarantee", "Unhappy with your server? We provide a 48 hour money-back guarantee on 
                    all game servers.", "bt-check-circle")

            );

            while ($i < count($features)) {
                echo '
                <div class="col-md-4 col-sm-6">
                    <div class="feature-item">
                        <div class="features-image">
                            <i class="btl '.$features[$i][2].' bt-stack-1x"></i>
                        </div>
                        <div class="features-title">
                            '.$features[$i][0].'
                        </div>
                        <div class="features-description">
                            '.$features[$i][1].'
                        </div>
                    </div>
                </div>
                ';
                $i++;
            };
            ?>
        </div>
        <div class="currency-disclaimer">
            <div class="disclaimer-text">
                All prices are listed in AUD</div> <img src="../assets/img/australia-flag.png">
        </div>

    </div>
</section>

<?php include("../assets/footer.inc"); ?>
