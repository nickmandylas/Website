<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - Factorio</title>
</head>

<?php include("../assets/header.inc"); ?>
<?php include("../assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="factorio">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="lighter-transparent-box">
                    <h1>Factorio</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">

        <hr>
        <div class="row">

            <?php
            $i = 0;
            $features = array(
                array("Instant Setup", "Your game server will be setup straight after payment. Don't wait for 
                    bureaucracy, play instantly.", "bt-bolt"),
                array("DDOS Protection", "Full DDOS protection and mitigation is included on all games to minimise any 
                    downtime and maximise game time.", "bt-shield"),
                array("1-Click Mod Installer", "Enjoy the ease of installing hundreds of game modifications 
                    instantaneously at a single click.", "bt-download"),
                array("Full Control", "We give you full control, customise every aspect from the gamemode it runs to 
                    the IP you connect to.", "bt-crown"),
                array("Awesome Support", "We're available to 24/7 to answer any questions or problems that you may have. 
                    We've got your back!", "bt-wrench"),
                array("Money-back Guarantee", "Unhappy with your server? We provide a 48 hour money-back guarantee on 
                    all game servers.", "bt-check-circle")

            );

            while ($i < count($features)) {
                echo '
                <div class="col-md-4 col-sm-6">
                    <div class="feature-item">
                        <div class="features-image">
                            <i class="btl '.$features[$i][2].' bt-stack-1x"></i>
                        </div>
                        <div class="features-title">
                            '.$features[$i][0].'
                        </div>
                        <div class="features-description">
                            '.$features[$i][1].'
                        </div>
                    </div>
                </div>
                ';
                $i++;
            };
            ?>
        </div>
        <div class="currency-disclaimer">
            <div class="disclaimer-text">
                All prices are listed in AUD</div> <img src="../assets/img/australia-flag.png">
        </div>

    </div>
</section>

<?php include("../assets/footer.inc"); ?>
