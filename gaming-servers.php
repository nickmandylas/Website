<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - Gaming Servers</title>
</head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="gaming-servers">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="lighter-transparent-box">
                    <h1>Gaming Servers</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            Popular Titles
        </div>
        <div class="game-server-pop-list">
            <div class="row">

                <?php
                $i = 0;
                $pop_games = array(
                    array("Minecraft", "assets/img/game-server-logos/minecraft.jpg", "minecraft", "Minecraft Logo"),
                    array("Conan Exiles", "assets/img/game-server-logos/conanexiles.png", "conan-exiles", "Conan Exiles
                     Logo"),
                    array("Rust", "assets/img/game-server-logos/rust.jpg", "rust", "Rust Logo"),
                    array("Ark: Se", "assets/img/game-server-logos/ark-survival.jpg", "ark-survival", "ARK: Survival 
                    Evolved Logo")
                );

                while ($i < count($pop_games)) {
                    echo '
                    <div class="col-lg-3 col-md-6 col-6">
                        <a href="/games/'.urlencode($pop_games[$i][2]).'.php">
                            <div class="game-server-pop-item">
                                <div class="game-server-picture">
                                    <img src="'.$pop_games[$i][1].'" alt="'.$pop_games[$i][3].'">
                                </div>
                                <div class="game-server-pop-title">
                                    '.$pop_games[$i][0].'
                                </div>
                            </div>
                        </a>
                    </div>
                    ';
                    $i++;
                };

                ?>
            </div>
        </div>
        <hr>
        <div class="content-title">
            Game List
        </div>
        <div class="game-server-list">
            <div class="row">

                <?php
                $i = 0;
                $games = array(
                        array("ARK: SURVIVAL EVOLVED", "$11.00", "ark-survival-logo", "ark-survival"),
                        array("ARMA II / III", "$12.00", "arma-3-logo", "arma"),
                        array("CLASSIC OFFENSIVE (CS)", "$5.00", "classic-offensive-logo", "classic-offensive"),
                        array("CONAN EXILES", "$10.00", "conan-exiles-logo", "conan-exiles"),
                        array("COUNTER STRIKE:GO", "$5.00", "global-offensive-logo", "global-offensive"),
                        array("DON'T STARVE TOGETHER", "$6.00", "dont-starve-logo", "dont-starve"),
                        array("FACTORIO", "$11.00", "factorio-logo", "factorio"),
                        array("GARRY'S MOD", "$5.00", "garrys-mod-logo", "garrys-mod"),
                        array("HURTWORLD", "$5.00", "hurtworld-logo", "hurtworld"),
                        array("INSURGENCY", "$6.00", "insurgency-logo", "insurgency"),
                        array("MINECRAFT", "$2.50", "minecraft-logo", "minecraft"),
                        array("MINECRAFT: POCKET EDITION", "$3.00", "minecraft-pocket-edition-logo", "minecraft-pe"),
                        array("RUST", "$9.00", "rust-logo", "rust"),
                        array("SPACE ENGINEERS", "$10.00", "space-engineers-logo", "space-engineers"),
                        array("STARBOUND", "$8.00", "starbound-logo", "starbound"),
                        array("STARMADE", "$5.00", "starmade-logo", "starmade"),
                        array("TEAM FORTRESS 2", "$5.00", "team-fortress-2-logo", "team-fortress-2"),
                        array("TERRARIA", "$6.00", "terraria-logo", "terraria"),
                );

                while ($i < count($games)) {
                    echo '
                    <div class="col-md-2 hidden-sm-down">
                        <div class="game-server-item-logo" id="'.$games[$i][2].'">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="game-server-item">
                            <div class="row">
                                <div class="game-server-title col-md-5 col-sm-12">
                                    '.$games[$i][0].'
                                </div>
                                <div class="game-server-info col-md-7 col-sm-12">
                                    <div class="game-server-pricing">
                                        From <i>'.$games[$i][1].'</i> Per Month
                                    </div>
                                    <span class="line-break-1200px"></span>
                                    <a class="button blue-button" href="/games/'.urlencode($games[$i][3]).'.php">
                                        <i class="btl bt-info-circle bt-stack-1x"></i>
                                        MORE INFO
                                    </a>
                                    <a class="button orange-button" href="#">
                                        <i class="btl bt-shopping-cart bt-stack-1x"></i>
                                        ORDER NOW
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    ';
                    $i++;
                };
                ?>
            </div>
        </div>
        <hr>
        <div class="row">

            <?php
            $i = 0;
            $features = array(
                    array("Instant Setup", "Your game server will be setup straight after payment. Don't wait for 
                    bureaucracy, play instantly.", "bt-bolt"),
                    array("DDOS Protection", "Full DDOS protection and mitigation is included on all games to minimise any 
                    downtime and maximise game time.", "bt-shield"),
                    array("1-Click Mod Installer", "Enjoy the ease of installing hundreds of game modifications 
                    instantaneously at a single click.", "bt-download"),
                    array("Full Control", "We give you full control, customise every aspect from the gamemode it runs to 
                    the IP you connect to.", "bt-crown"),
                    array("Awesome Support", "We're available to 24/7 to answer any questions or problems that you may have. 
                    We've got your back!", "bt-wrench"),
                    array("Money-back Guarantee", "Unhappy with your server? We provide a 48 hour money-back guarantee on 
                    all game servers.", "bt-check-circle")

            );

            while ($i < count($features)) {
                echo '
                <div class="col-md-4 col-sm-6">
                    <div class="feature-item">
                        <div class="features-image">
                            <i class="btl '.$features[$i][2].' bt-stack-1x"></i>
                        </div>
                        <div class="features-title">
                            '.$features[$i][0].'
                        </div>
                        <div class="features-description">
                            '.$features[$i][1].'
                        </div>
                    </div>
                </div>
                ';
                $i++;
            };
            ?>
        </div>
        <div class="currency-disclaimer">
            <div class="disclaimer-text">
                All prices are listed in AUD</div> <img src="../assets/img/australia-flag.png">
        </div>
    </div>
</section>

<?php include("assets/footer.inc"); ?>
