## Hydra Servers - Website
Landing website for Hydra Servers ([hydraservers.com.au](http://hydraservers.com.au)).

### License
Content on this page has been developed by Nick Mandylas for Hydra Servers ([hydraservers.com.au]
(http://hydraservers.com.au)). Written permission from admin@hydraservers.com.au is required for third-party use of 
any materials contained herein.

### Credits
Bootstrap - [license](https://github.com/twbs/bootstrap/blob/master/LICENSE) - [homepage](http://getbootstrap.com)

FontAwesome - [license](http://fontawesome.io/license/) - [homepage](http://fontawesome.io)

FontAwesome Animations - [license](https://github.com/l-lin/font-awesome-animation#license) - [homepage](https://github.com/l-lin/font-awesome-animation)

jQuery - [license](https://github.com/jquery/jquery/blob/master/LICENSE.txt) - [homepage](http://jquery.com)

