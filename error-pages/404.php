<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - 404 Error!</title>
</head>

<?php include("../assets/header.inc"); ?>
<?php include("../assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="green-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="transparent-box">
                    <h1>404 Error!</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            Oops! 404 Error<br>
            <i>The page you are looking for does not exist!</i>
        </div>
        <div class="paragraph">
            Depending on who is to blame (hopefully it's not us), you've stumbled onto a page that doesn't exist!<br>
            It can range from a few different issues:
            <ul class="arrow-list" style="margin-top: 0;">
                <li>The page you were looking for has moved.</li>
                <li>You've entered the incorrect address for your specified page.</li>
                <li>We've done goofed.</li>
            </ul>
            Now don't worry! Feel free to contact <a href="http://clients.hydraservers.com.au">support</a> or live
            chat if you need help.
        </div>
    </div>
</section>

<?php include("../assets/footer.inc"); ?>
