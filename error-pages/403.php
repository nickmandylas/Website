<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - 403 Error!</title>
</head>

<?php include("../assets/header.inc"); ?>
<?php include("../assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="red-yellow">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="transparent-box">
                    <h1>403 Error!</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            Oops! 403 Error<br>
            <i>You're not allowed to be here!</i>
        </div>
        <div class="paragraph">
            It appears you're trying to snoop around, trying to find some clues. <br>
            You're not allowed to be here however. Click <a href="<?php url("index"); ?>">here</a> to return
            back home.
        </div>
    </div>
</section>

<?php include("../assets/footer.inc"); ?>
