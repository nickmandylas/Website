<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>
<!-- NAVIGATION -->
<section class="navigation">
    <nav class="navbar navbar-toggleable-md mb-4">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i class="btl bt-bars bt-stack-1x"></i>
                </span>
            </button>
            <a class="navbar-brand" href="<?php url("index"); ?>">
                <img src="../assets/img/navbar-logo.png">
                <h3>
                    Hydra
                    <i>Servers</i>
                </h3>
            </a>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav navbar-right">
                    <li class="nav-item">
                        <a class="navlink" href="<?php url("gaming-servers"); ?>">GAMING SERVERS</a>
                    </li>
                    <li class="nav-item">
                        <a class="navlink" href="<?php url("dedicated"); ?>">DEDICATED</a>
                    </li>
                    <li class="nav-item">
                        <a class="navlink" href="<?php url("web"); ?>">WEB</a>
                    </li>
                    <li class="nav-item">
                        <a class="navlink" href="<?php url("voice"); ?>">VOICE</a>
                    </li>
                    <li>
                        <a class="navlink button blue-button" href="https://clients.hydraservers.com.au">
                            <i class="btl bt-user bt-stack-1x"></i>
                            CLIENT AREA
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>
