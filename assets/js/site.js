/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */

console.log("Wow! You must know your stuff! We always welcome new developers onto the team. Please" +
    " email careers@hydraservers.com.au with your CV (and preferably portfolio) to have a chance to work with us.");

function gameModList(id) {
    $('gamemodlist_1').hide();
    $('gamemodlist_2').hide();
    $('gamemodlist_3').hide();
    $('gamemodlist_4').hide();

    $('gamemod_list_' + id).fadeIn();
}

function voice_server_checkout(id, cart_id, total) {
    /* TODO - Complete WHMCS integration */
}

if(window.screen.width >= 768 && window.screen.width <= 992) {
    $('#text-minimised').show();
} else {
    $('#text-extended').show();
}

window.addEventListener('resize', function(event){
    if(window.screen.width >= 768 && window.screen.width <= 992) {
        $('#text-extended').hide();
        $('#text-minimised').show();
    } else {
        $('#text-minimised').hide();
        $('#text-extended').show();
    }
});

/* Time set for weekly deal
var countDownDate = new Date("Feb 24, 2018 23:59:59").getTime();

var x = setInterval(function() {

    var now = new Date().getTime();
    var distance = countDownDate - now;

    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";

    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "OFFER EXPIRED";
    }
}, 1000);

*/