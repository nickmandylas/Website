<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>
<!-- FOOTER -->
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-6">
                <div class="heading">
                    SHOP NOW
                    <div class="footer-divide"></div>
                </div>
                <ul class="footer-list">
                    <li><a href="<?php url("gaming-servers"); ?>">Game Servers</a></li>
                    <li><a href="<?php url("dedicated"); ?>">Dedicated Servers</a></li>
                    <li><a href="<?php url("web"); ?>">Website Hosting</a></li>
                    <li><a href="<?php url("voice"); ?>">Voice Servers</a></li>
                    <li><a href="<?php url("gift-cards"); ?>">Gift Cards</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-6">
                <div class="heading">
                    COMPANY
                    <div class="footer-divide"></div>
                </div>
                <ul class="footer-list">
                    <li><a href="<?php url("about-us"); ?>">About Us</a></li>
                    <li><a href="<?php url("datacentre"); ?>">Data Centre</a></li>
                    <li><a href="<?php url("faq"); ?>">FAQs</a></li>
                    <li><a href="<?php url("careers"); ?>">Careers</a></li>
                    <li><a href="<?php url("terms-of-service"); ?>">Terms of Service</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-6">
                <div class="heading">
                    SUPPORT
                    <div class="footer-divide"></div>
                </div>
                <ul class="footer-list">
                    <li><a href="">Contact Us</a></li>
                    <li><a href="#">Support Tickets</a></li>
                    <li><a href="#">Knowledgebase</a></li>
                    <li>
                        <a href="#">
                            System Status
                            <img src="../assets/img/status-online.png">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-6 footer-right">
                <a href="<?php url(""); ?>">
                    <img class="footer-logo" src="../assets/img/footer-logo.png"><br>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- SUB FOOTER -->
<section class="subfooter">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12" style="padding-top:9px;">
                Copyright &copy; Hydra Servers 2014 - 2017.<span class="line-break-992px"></span> ABN: 25 690 192 012
            </div>
            <div class="col-md-6 col-sm-12 footer-right">
                <img src="../assets/img/footer-payment-logos.png">
            </div>
        </div>
    </div>
</section>

<!-- SCRIPTS -->
<script src="../assets/js/jquery-3.1.1.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/site.js"></script>

</body>
</html>
