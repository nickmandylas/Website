<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 *
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Hydra Servers">

    <script src="https://use.typekit.net/rkf5qli.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <link href="../assets/css/black-tie.min.css" rel="stylesheet">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <link rel="icon" href="../assets/img/full-logo.png?" type="image/x-icon">

</head>
<body>

<?php
function url($domain = false) {
    $site_status = "localhost";
    if ($site_status == "localhost") {
        if ($domain == false) {
            echo "http://localhost:8087/HydraServers";
        } else {
            echo "http://localhost:8087/HydraServers/" . $domain . ".php";
        }
    } else if ($site_status == "dev-server") {
        if ($domain == false) {
            echo "http://dev.hydraservers.com.au";
        } else {
            echo "http://dev.hydraservers.com.au/" . $domain . ".php";
        }
    } else {
        if ($domain == false) {
            echo "http://hydraservers.com.au";
        } else {
            echo "http://hydraservers.com.au/" . $domain . ".php";
        }
    }
}
?>