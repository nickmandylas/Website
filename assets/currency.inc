<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */

function currency($gbp, $to) {

    if ($to == "AUD" OR $to == "") {

        echo "$" . $gbp;

    } else {

        $url = "http://rate-exchange.appspot.com/currency?from=AUD&to=" . $to . "&q=" . $gbp;

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($data, true);

        if ($to == "USD") {

            $sym = "$";

        } else {

            $sym = "$";

        }

        return $sym . round($result['v'], 2);

    }

    return null;
}

?>