<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - About Us</title>
</head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="green-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="transparent-box">
                    <h1>About Us</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            ABOUT US<br>
            <i>Melbourne, Australia</i>
        </div>
        <div class="paragraph">
            <div class="text">
                Hydra Servers was founded in June 2014 and is a registered Australian business. Based in Melbourne,
                our core mission was to provide the best low cost services with the best quality possible. Opening
                with web hosting services, we expanded to offering gaming servers for a variety of titles such as
                Minecraft, ARK Survival and Counter-Strike as well as services such as Teamspeak3 voice servers.
                Following customer requests we've begun providing Dedicated Servers located at our premier facility
                NEXTDC M1.
            </div>
        </div>
        <div class="paragraph">
            <div class="text">
                Hydra Servers will work along side you to create your own community or personal website,
                without the hurdles that occur with larger providers. We might not be the biggest hosting
                provider in the world, but we see that as a benefit. We're small enough to keep in touch with
                our customers, whilst having the power to scale to demand.
            </div>
        </div>
        <div class="paragraph">
            <div class="text">
                Hydra Servers only utilises the best hardware required for the specific service. Moreover, all our
                equipment is company owned so you can rest assured we are in control of your server, not a reseller.
                Soon we will begin to run our own network (Insert ASN) and begin to hold allocations directly with
                APNIC for IP space.
            </div>
        </div>
        <hr>
        <div class="content-title">
            Meet the Team
        </div>
        <div class="paragraph">
            <div class="text">
                Hydra Servers is a locally owned and operated business. We play a lot of games.
            </div>
        </div>
        <div class="team-profiles">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="team-profile">
                        <div class="team-picture">
                            <img src="assets/img/team-images/nick-image.png">
                        </div>
                        <div class="team-name">
                            Nick Mandylas<br>
                            <i>Founder & CEO</i>
                        </div>
                        <div class="team-description">
                            Currently studying computer science, Nick is a competitive gamer frequently playing
                            CSGO. (He is currently ranked GN3).
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="team-profile">
                        <div class="team-picture">
                            <img src="assets/img/team-images/frank-image.PNG">
                        </div>
                        <div class="team-name">
                            Frank Taylor<br>
                            <i>Support Technician</i>
                        </div>
                        <div class="team-description">
                            A math expert & engineering lover, Frank loves to regularly dominate playing CIV 6.
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="team-profile">
                        <div class="team-picture">
                            <img src="assets/img/team-images/peter-image.png">
                        </div>
                        <div class="team-name">
                            Peter Stamford<br>
                            <i>Support Technician</i>
                        </div>
                        <div class="team-description">
                            A survival fanatic, Peter plays Minecraft and ARK everyday. He has just started a fresh world,
                            aiming to get elytra wings.
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="team-profile">
                        <div class="team-picture">
                            <img src="assets/img/team-images/unknown-image.png">
                        </div>
                        <div class="team-name">
                            You?<br>
                            <i>We're Hiring!</i>
                        </div>
                        <div class="team-description">
                            We're looking for motivated and talented individuals who want to work in the hosting
                            industry.<br>
                            <a class="button blue-button" href="<?php url("careers"); ?>">
                                VIEW POSITIONS
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("assets/footer.inc"); ?>
