<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - Datacentre</title>
</head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="red-yellow">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="transparent-box">
                    <h1>Data Centre & Network</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            DATA CENTRE<br>
            <i>NEXTDC M1 - 826 Lorimer St, Port Melbourne VIC 3207</i>
        </div>
        <div class="paragraph">
            <div class="text">
                This is our main facility, which holds our enterprise equipment. Located near the CBD in Port Melbourne,
                NEXTDC's M1 facility is a certified UTI Tier III facility. M1 is protected by advanced physical security
                and protocols, including biometric fingerprint technology and 24/7 on-site security personal. With the
                level of security on site, M1 is one of the most secure data centers in Australia.
            </div>
        </div>
        <div class="paragraph">
            <div class="text">
                As a result of the facility being UTI Tier III, all critical systems are N+1, ensuring ultimate levels
                of service availability. Furthermore, it is backed by a 100% no-break power SLA agreement. Rest assured,
                your services will continue to be online, no matter the severity of the situation.
            </div>
        </div>
        <div class="call-to-action">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <b>Need reassurance?</b> <br> Test our network through our looking glass page.
                </div>
                <div class="call-to-action-button col-md-6 col-sm-12">
                    <a class="button blue-button" href="#">
                        <i class="btl bt-info-circle bt-stack-1x"></i>
                        TEST NOW
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("assets/footer.inc"); ?>
