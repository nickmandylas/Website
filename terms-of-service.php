<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

    <!-- HEADER -->
    <head>
        <title>Hydra Servers - Terms Of Service</title>
    </head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="dark-blue-aqua">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="transparent-box">
                    <h1>Terms of Service</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            Terms of Service<br>
            <i>Last updated: 13/01/2017</i>
        </div>
        <div class="paragraph">
            <div class="text">
                The following terms of service ("TOS") will apply to the agreement to provide service between
                Hydra Servers ("Company"), and the individual or business entity identified on the order form for
                said services ("Customer"). These terms, shall hereinafter be referred to as the "Agreement" between
                the two above mentioned parties.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Service Rates
            </div>
            <div class="text">
                Customer acknowledges that they have been made adequately aware of the initial rates and fees
                associated with services being rendered by the Company and have received a complete description of
                services to be rendered. Customer also acknowledges that the Company reserves the right to change the
                specified rates and charges from time to time. Any promotional offers made by the Company are
                contingent upon the Company maintaining its cost of service goals, including but not limited to rates
                charged by its suppliers.
                <ul class="arrow-list">
                    <li>A monthly charge will be applied on the anniversary of provisioning of the service with us.</li>
                    <li>Should your service be provisioned later (after the registration/sign up), the provisioning
                        date can be altered accordingly.</li>
                    <li>Where manual work is required by a member of Hydra Servers, an administration fee may be
                        applied before completing the requested service.</li>
                    <li>A request for a chargeback will attract a $35 administration fee and will be charged to the
                        account and must be paid in full before a chargeback is completed.</li>
                    <li>Refunds are provided at the discretion of the Hydra Servers Staff. It is at the discretion
                        of Hydra Servers Staff to refund the amount to a nominated bank account/PayPal account.</li>
                    <li>A monthly charge will be applied on the anniversary of provisioning of the service with us.</li>
                    <li>Fees, payments and/or cost such as setup/installation fees, domain registration costs,
                        and SSL certificate costs are non-refundable.</li>
                    <li>Should a request be made for a copy of any existing backed up data to be supplied to you
                        from any of our infrastructure, an administration fee may be applied and must be paid in
                        full before any data is provided to the owner of the account.</li>
                    <li>All prices that are advertised on our website and any advertisements created by Hydra
                        Servers may exclude Credit Card Fees.</li>
                    <li>We reserve the right to charge per gigabyte for any excessive bandwidth usage that exceeds
                        your monthly allowance that has been described in the product information on our website
                        or promotions.</li>
                    <li>Where we become liable for any cost incurred by our supplier that relates to your service,
                        you become liable for the total cost plus interest.</li>
                </ul>
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Payments and Fees
            </div>
            <div class="text">
                <ul class="arrow-list" style="margin-top: 0;">
                    <li>Payments accepted are PayPal, Credit Card (Using the Stripe Gateway) or Bank Transfer.</li>
                    <li>Invoices are automatically generated 15 days prior to the invoice due date to the registered
                        email address.</li>
                    <li>An invoice reminder will be generated and sent to the registered email address 1 and 2 days
                        prior to the invoice due date.</li>
                    <li>An overdue invoice reminder will be sent on the first day after the invoice due date to the
                        registered email address.</li>
                    <li>A third overdue invoice reminder will be sent on the third day after the invoice due
                        date to the registered email address.</li>
                </ul>
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Money Back Guarantee
            </div>
            <div class="text">
                Hydra Servers provides a 48 Hour Money back guarantee on all services, no questions asked.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Billing Disputes
            </div>
            <div class="text">
                <ul class="arrow-list" style="margin-top:0;">
                    <li>We will commence supplying of the requested service to you within 2 working days from the
                        day the payment has been submitted.</li>
                    <li>If a service is advertised with automatic provisioning, this service will be completed
                        within no more than 30 minutes from the time the service has been confirmed as paid by
                        our billing system.</li>
                    <li>If a field has not been filled correct when submitting your form, this can prevent the
                        service from being provisioned.</li>
                    <li>We will supply all of the information that is necessary for the customer to access the
                        requested service(s) once provisioning of the service has been completed.</li>
                </ul>
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Indemnification
            </div>
            <div class="text">
                Under no circumstances shall the Company be held liable for damages resulting from any interruption of
                service for an amount greater than the amount of the charges payable by the Customer for services
                during the period damages occurred. Customer also acknowledges that in no case will the Company be
                liable for damages as a result of its own negligence in excess of the charges payable by the Customer
                for services during the period damages occurred. The customer acknowledges that they make use of the
                Company's services and facilities at their own risk.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Accepting an Order
            </div>
            <div class="text">
                <ul class="arrow-list">
                    <li>That we find you eligible for the service.</li>
                    <li>The service is available and that we have stock available.</li>
                    <li>Personal information that you have supplied to us during the application process is
                        correct.</li>
                    <li>Any previous service(s) that you have held with us was accepted and has no outstanding
                        invoices.</li>
                </ul>
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                No Lease Implied
            </div>
            <div class="text">
                Customer acknowledges that the Company is providing a service, and no binding lease of physical
                equipment or real estate is in any way implied as part of this agreement.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Privacy Policy
            </div>
            <div class="text">
                Company will not sell, lease, borrow, give, or otherwise dispose of any type of customer provided
                information to any third party unless compelled to do so by law or in cooperation with any law
                enforcement investigation. Company reserves the right to collect and utilize any customer information,
                including, but not limited to email addresses and web site cookies, for internal tracking and/or
                marketing purposes.
            </div>
        </div>
        <div class="paragraph">
            <div class="heading">
                Contact Us
            </div>
            <div class="text">
                You are able to get in touch with us by:<br>
                (Snail) Mail - Hydra Servers, P.O Box 12400 Middle Camberwell, Victoria, 3124<br>
                Email - <a href="mailto:admin@hydraservers.com.au">admin@hydraservers.com.au</a><br>
                Phone - (03) 9018 7378
            </div>
        </div>
    </div>
</section>

<?php include("assets/footer.inc"); ?>