<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - Australian Hosting</title>
</head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- CONTENT -->
<section class="content">
    <div class="container">

    </div>
</section>

<?php include("assets/footer.inc"); ?>
