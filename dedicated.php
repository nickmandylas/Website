<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - Dedicated Servers</title>
</head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="dedicated-servers">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="lighter-transparent-box">
                    <h1>Dedicated Servers</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="paragraph">
            <div class="heading">
                Single CPU Dedicated Servers
            </div>
            <div class="text">
                Includes <b style="color: #01549b">Server Management</b> &amp; 5Gbps DDoS Protection
            </div>
            <table class="server-info">
                <thead>
                <th>CPU Series</th>
                <th>RAM</th>
                <th>STORAGE</th>
                <th>TRANSFER</th>
                <th>PRICE</th>
                <th></th>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <p>Intel® Xeon® <b>E3-1230v3</b></p>
                        <p>4 CPU Cores @ 3.4Ghz</p>
                    </td>
                    <td>
                        16GB DDR3
                    </td>
                    <td>
                        <p>120GB SSD or</p>
                        <p>500GB HDD</p>
                    </td>
                    <td>
                        <p>25TB</p>
                        <p>Gig-E Port</p>
                    </td>
                    <td class="server-info-price">
                        <p style="font-size: 1.15rem;">$130.00</p>
                        <p>Fully Managed</p>
                    </td>
                    <td>
                        <a class="button blue-button" href="<?php url(""); ?>">
                            CONFIGURE
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Intel® Xeon® <b>E3-1245v5</b></p>
                        <p>4 CPU Cores @ 3.5Ghz</p>
                    </td>
                    <td>
                        32GB DDR3
                    </td>
                    <td>
                        <p>240GB SSD or</p>
                        <p>2TB HDD</p>
                    </td>
                    <td>
                        <p>25TB</p>
                        <p>Gig-E Port</p>
                    </td>
                    <td class="server-info-price">
                        <p style="font-size: 1.15rem;">$240.00</p>
                        <p>Fully Managed</p>
                    </td>
                    <td>
                        <a class="button red-button" href="<?php url(""); ?>">
                            SOLD OUT
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Intel® Xeon® <b>E5-1650v3</b></p>
                        <p>6 CPU Cores @ 3.6Ghz</p>
                    </td>
                    <td>
                        64GB DDR3
                    </td>
                    <td>
                        <p>240GB SSD or</p>
                        <p>4TB HDD</p>
                    </td>
                    <td>
                        <p>25TB</p>
                        <p>Gig-E Port</p>
                    </td>
                    <td class="server-info-price">
                        <p style="font-size: 1.15rem;">$470.00</p>
                        <p>Fully Managed</p>
                    </td>
                    <td>
                        <a class="button blue-button" href="<?php url(""); ?>">
                            CONFIGURE
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="paragraph">
            <div class="heading">
                Dual CPU Dedicated Servers
            </div>
            <div class="text">
                Includes <b style="color: #01549b">Server Management</b> &amp; 10Gbps DDoS Protection
            </div>
            <table class="server-info">
                <thead>
                <th>CPU Series</th>
                <th>RAM</th>
                <th>STORAGE</th>
                <th>TRANSFER</th>
                <th>PRICE</th>
                <th></th>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <p>Intel® Xeon® <b>L5620</b></p>
                        <p>12 CPU Cores @ 2.0Ghz</p>
                    </td>
                    <td>
                        48GB DDR3
                    </td>
                    <td>
                        <p>480GB SSD or</p>
                        <p>4TB HDD</p>
                    </td>
                    <td>
                        <p>25TB</p>
                        <p>Gig-E Port</p>
                    </td>
                    <td class="server-info-price">
                        <p style="font-size: 1.15rem;">$350.00</p>
                        <p>Fully Managed</p>
                    </td>
                    <td>
                        <a class="button blue-button" href="<?php url(""); ?>">
                            CONFIGURE
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Intel® Xeon® <b>E5-2660</b></p>
                        <p>16 CPU Cores @ 2.2Ghz</p>
                    </td>
                    <td>
                        64GB DDR3
                    </td>
                    <td>
                        <p>1024GB SSD or</p>
                        <p>4TB HDD</p>
                    </td>
                    <td>
                        <p>25TB</p>
                        <p>Gig-E Port</p>
                    </td>
                    <td class="server-info-price">
                        <p style="font-size: 1.15rem;">$490.00</p>
                        <p>Fully Managed</p>
                    </td>
                    <td>
                        <a class="button blue-button" href="<?php url(""); ?>">
                            CONFIGURE
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Intel® Xeon® <b>E5-2630v4</b></p>
                        <p>20 CPU Cores @ 2.2Ghz</p>
                    </td>
                    <td>
                        128GB DDR3
                    </td>
                    <td>
                        <p>2TB SSD or</p>
                        <p>8TB HDD</p>
                    </td>
                    <td>
                        <p>25TB</p>
                        <p>Gig-E Port</p>
                    </td>
                    <td class="server-info-price">
                        <p style="font-size: 1.15rem;">$730.00</p>
                        <p>Fully Managed</p>
                    </td>
                    <td>
                        <a class="button red-button" href="<?php url(""); ?>">
                            SOLD OUT
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="paragraph">
            <div class="heading">
                OUR NETWORK
            </div>
            <div class="text">
                All hardware is rented from Hydra Servers is owned and operated in a TIII Facility.
            </div>
            <div class="call-to-action" style="border-radius: 0; box-shadow: none;">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <b style="color: #01549b">TEST YOUR CONNECTION</b> <br> Test our Gigabit network, we're sure you'll be surprised.
                    </div>
                    <div class="call-to-action-button col-md-6 col-sm-12">
                        <a class="button blue-button" href="<?php url("datacentre"); ?>">
                            <i class="btl bt-info-circle bt-stack-1x"></i>
                            MORE INFO
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <?php
            $i = 0;
            $features = array(
                array("24HR Provisioning", "We will provision your server within 24 hours guaranteed. If we're unable too,
                    you'll receive a month free!", "bt-bolt"),
                array("DDOS Protection", "Full DDOS protection and mitigation is included on all games to minimise any 
                    downtime and maximise game time.", "bt-shield"),
                array("100's of Templates", "Choose from over 100 OS and application templates, allowing you to get up 
                    and running quickly.", "bt-download"),
                array("IPMI Remote KVM Console", "All managed servers include full KVM over IP remote console via IPMI 
                    or DRAC, giving you control.", "bt-crown"),
                array("Awesome Support", "We're available to 24/7 to answer any questions or problems that you may have. 
                    We've got your back!", "bt-wrench"),
                array("Money-back Guarantee", "Unhappy with your server? We provide a 48 hour money-back guarantee on 
                    all game servers.", "bt-check-circle")

            );

            while ($i < count($features)) {
                echo '
                <div class="col-md-4 col-sm-6">
                    <div class="feature-item">
                        <div class="features-image">
                            <i class="btl '.$features[$i][2].' bt-stack-1x"></i>
                        </div>
                        <div class="features-title">
                            '.$features[$i][0].'
                        </div>
                        <div class="features-description">
                            '.$features[$i][1].'
                        </div>
                    </div>
                </div>
                ';
                $i++;
            };
            ?>
        </div>
        <div class="currency-disclaimer" style="margin-top: 1.2rem;">
            <div class="disclaimer-text">
                All prices are listed in AUD</div> <img src="../assets/img/australia-flag.png">
        </div>
    </div>
</section>

<?php include("assets/footer.inc"); ?>
