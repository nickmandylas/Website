<?php
/**
 * Content on this page has been developed by Nick Mandylas for Hydra Servers (http://hydraservers.com.au).
 * Written permission from admin@hydraservers.com.au is required for third-party use of any materials
 * contained herein.
 *
 * Author: Nick Mandylas
 * Date: Jan/2017
 */
?>

<!-- HEADER -->
<head>
    <title>Hydra Servers - Careers</title>
</head>

<?php include("assets/header.inc"); ?>
<?php include("assets/navigation.inc"); ?>

<!-- PAGE TITLE -->
<section class="page-title" id="purple-aqua">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="transparent-box">
                    <h1>Careers</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTENT -->
<section class="content">
    <div class="container">
        <div class="content-title">
            Careers<br>
            <i>Want to work for Hydra Servers?</i>
        </div>
        <div class="paragraph">
            <div class="heading">
                Support Representative (TIER I)
            </div>
            <div class="content-title">
                <i>Responsibilities</i>
            </div>
            <div class="text">
                <ul class="arrow-list">
                    <li>Provide support in an increasingly fast paced environment, where hundreds of tickets are
                    submitted each month.</li>
                    <li>Work alongside co-workers in a team environment, to provide efficient and quick support.</li>
                    <li>Provide status updates to management / co-workers when requested.</li>
                </ul>
            </div>
            <div class="content-title">
                <i>Requirements</i>
            </div>
            <div class="text">
                <ul class="arrow-list">
                    <li>Must be a permenant Australian citizen. (Highly preferred from Melbourne). </li>
                    <li>Must be aged 18 years or older.</li>
                    <li>Strong understanding of English, to communicate coherently and efficiently.</li>
                    <li>Able to communicate via Slack and Skype.</li>
                </ul>
            </div>
            <div class="content-title">
                <i>Helpful Attributes</i>
            </div>
            <div class="text">
                <ul class="arrow-list">
                    <li>Previous experience using WHMCS.</li>
                    <li>Previous expiernce using Multicraft/Minecraft server administration.</li>
                    <li>Experience using communication tools such as Skype and Slack.</li>
                    <li>Basic understanding of UNIX based systems.</li>
                    <li>Hold an ABN (Australian Business Number).</li>
                </ul>
            </div>
            <div class="content-title">
                <i>Benefits (The good stuff!)</i>
            </div>
            <div class="text">
                <ul class="arrow-list">
                    <li>Competitive pay rate. ($25/hr +)</li>
                    <li>Receive paid training (3 - 4 hours).</li>
                    <li>Work from home! No need to get dressed in the morning (we prefer you do) and jump onto the
                    computer. Or you can work on the go!</li>
                    <li>Weekly paid takeaway! One day a week, we'll front the bill from any restaurant/take-out place of
                    your choice! Nom nom.</li>
                </ul>
            </div>
            <div class="content-title">
                <i>Apply</i>
            </div>
            <div class="text">
                Still interested? Please send your cover letter, resume and available hours to
                <a href="mailto:careers@hydraservers.com.au">careers@hydraservers.com.au</a>.
            </div>
        </div>
    </div>
</section>

<?php include("assets/footer.inc"); ?>

